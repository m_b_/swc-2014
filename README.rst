Software-Challenge Client
=========================

Basics
------

Dieses Repository laesst sich mit Hilfe von git clonen::

    git clone git@bitbucket.org:m_b_/swc-2014.git

Zusaetzlich muss noch der ``lib`` Order aus dem `offiziellen Simple Client
<http://www.informatik.uni-kiel.de/fileadmin/zentrale_bereiche/software_challenge/2014/downloads/simpleclient-si-src.zip>`_
in das Verzeichnis kopiert werden. Wahlweise kann man auch den ``doc`` Ordner mit
uebernehmen.

IDE
---

Als IDE bieten sich `IntelliJ <http://www.jetbrains.com/idea/>`_,
`Netbeans <https://netbeans.org/>`_ oder `Eclipse <http://www.eclipse.org/>`_ an.
IntelliJ ist die m. E.  die beste Wahl. Eclipse wird von der Uni empfohlen und
`hier <http://sc-doku.gfxpro.eu/wiki/Einrichtung_der_Entwicklungsumgebung_(Java)>`_
erlaeutert.

Testen
------

Zum Ausprobieren des eigenen Clients benoetigt man den Server. Dieser kann von
der Seite der Software-Challenge heruntergeladen werden.
`Link <http://www.informatik.uni-kiel.de/fileadmin/zentrale_bereiche/software_challenge/2014/downloads/server-gui.zip>`_

Im Server startet man ein neues Spiel, wobei der Spielertyp auf
``Computerspieler (manuell)`` gesetzt werden muss.

Nach dem Start des Spiels kann nun der Client in der IDE gestartet werden und
verbindet sich mit dem Server.
