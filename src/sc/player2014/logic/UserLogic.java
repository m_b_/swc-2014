package sc.player2014.logic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sc.player2014.Starter;
import sc.plugin2014.GameState;
import sc.plugin2014.entities.Field;
import sc.plugin2014.entities.Player;
import sc.plugin2014.entities.PlayerColor;
import sc.plugin2014.entities.Stone;
import sc.plugin2014.entities.StoneColor;
import sc.plugin2014.entities.StoneShape;
import sc.plugin2014.exceptions.InvalidMoveException;
import sc.plugin2014.exceptions.StoneBagIsEmptyException;
import sc.plugin2014.laylogic.PointsCalculator;
import sc.plugin2014.moves.DebugHint;
import sc.plugin2014.moves.ExchangeMove;
import sc.plugin2014.moves.LayMove;
import sc.plugin2014.moves.Move;
import sc.plugin2014.util.GameUtil;
import sc.shared.GameResult;

/**
 * Das Herz des Simpleclients: Eine sehr simple Logik,
 * die ihre Zuege zufaellig waehlt, aber gueltige Zuege macht.
 * Ausserdem werden zum Spielverlauf Konsolenausgaben gemacht.
 */
public class UserLogic implements sc.plugin2014.IGameHandler {

    private final Starter client;
    private GameState     gameState;
    private Player        currentPlayer;
    private Player		  otherPlayer;
    private long		  moveRequestedAt;

    /**
     * Erzeugt ein neues Strategieobjekt, das zufaellige Zuege taetigt.
     * 
     * @param client
     *            Der Zugrundeliegende Client der mit dem Spielserver
     *            kommunizieren kann.
     */
    public UserLogic(Starter client) {
        this.client = client;
    }

    /**
     * Wenn ein Zug von uns angefordert wurde, dann pr��fen wir erst, ob es der
     * aller erste Zug ist. Wenn dies nicht der Fall ist, wird ein zuf��lliger
     * Legezug gemacht. Wenn keine regelkonformen Z��ge gefunden wurden, dann
     * werden alle Steine von der Hand ausgetauscht.
     */
    @Override
    public void onRequestAction() {
    	this.moveRequestedAt = System.currentTimeMillis();
        System.out.println("*** Es wurde ein Zug angefordert");
        
        if (isOurStartLayMove()) {
            if (tryToDoStartLayMove() == false) {
                exchangeStones(currentPlayer.getStones());
            }
        }
        else {
            if (tryToDoValidLayMove() == false) {
                exchangeStones(currentPlayer.getStones());
            }
        }
    }

    /**
     * Gibt zur��ck, ob wir den aller ersten Zug machen m��ssen
     * 
     * @return <code>true</code>: Wir m��ssen den ersten Zug machen <br>
     *         <code>false</code>: Wir m��ssen nicht den ersten Zug machen
     */
    private boolean isOurStartLayMove() {
        return (!gameState.getBoard().hasStones())
                && (gameState.getStartPlayer() == currentPlayer);
    }

    /**
     * Versucht einen Startzug zu finden, indem die meiste Farbe der Steine auf
     * der Hand ausgew��hlt wird und dann versucht wird, ob das legen dieser
     * Reihe m��glich ist.
     * 
     * @return <code>true</code>: Wir m��ssen den ersten Zug machen <br>
     *         <code>false</code>: Wir m��ssen nicht den ersten Zug machen
     */
    private boolean tryToDoStartLayMove() {
        StoneColor bestStoneColor = GameUtil.getBestStoneColor(currentPlayer.getStones());
        StoneShape bestStoneShape = GameUtil.getBestStoneShape(currentPlayer.getStones());
        int bestStoneColorNr=0; //Anzahl der Steine mit der besten Farbe
        int bestStoneShapeNr=0; //Anzahl der Steine mit der besten Form
        List<Stone> Steinliste = currentPlayer.getStones();
        Stack<Stone> Shapestack = new Stack<Stone>(); //Stack mit den Steine der besten Form
        Stack<Stone> Colorstack = new Stack<Stone>(); //Stack mit den Steinen der besten Farbe
        List<Integer> usedShapes = new ArrayList<Integer>();
        List<Integer> usedColors = new ArrayList<Integer>();

        for (Stone stone : Steinliste) {
        	if(stone.getColor().ordinal() == bestStoneColor.ordinal() &&
        	!usedShapes.contains(stone.getShape().ordinal())) { //z��hlt die Steine mit der besten Farbe
        		bestStoneColorNr++;
        		Colorstack.push(stone);
        		usedShapes.add(stone.getShape().ordinal());
        	}
        
        	if(stone.getShape().ordinal() == bestStoneShape.ordinal() &&
        	!usedColors.contains(stone.getColor().ordinal())) { //z��hlt die Steine mit der besten Form
        		bestStoneShapeNr++;
        		Shapestack.push(stone);
        		usedColors.add(stone.getColor().ordinal());
        	}
        }        
        
        Boolean useColor = bestStoneColorNr > bestStoneShapeNr;
        
        if (bestStoneColorNr == bestStoneShapeNr){
    		int otherBestStoneColorNr=0; //Anzahl der Gegnersteine mit der besten Farbe
            int otherBestStoneShapeNr=0; //Anzahl der Gegnersteine mit der besten Form
            
            for(Stone stone : otherPlayer.getStones()) {
            	if(stone.getColor().ordinal() == bestStoneColor.ordinal()
            	&& !usedShapes.contains(stone.getShape().ordinal())) { //z��hlt die Steine mit der besten Farbe
            		otherBestStoneColorNr++;
            		usedShapes.add(stone.getShape().ordinal());
            	}
            
            	if(stone.getShape().ordinal() == bestStoneShape.ordinal()
            	&& !usedColors.contains(stone.getColor().ordinal())) { //z��hlt die Steine mit der besten Form
            		otherBestStoneShapeNr++;
            		usedColors.add(stone.getColor().ordinal());
            	}
            }
            
            useColor = otherBestStoneColorNr < otherBestStoneShapeNr;
    	}
        
        int startfieldnr = 7;
        LayMove startmove = new LayMove(); //Startzug-Konstruktor
        if (useColor){ //Hier ">" sobald die Gegneranalyse laeuft!
        	while(Colorstack.empty() == false){	
        		startmove.layStoneOntoField(Colorstack.pop(), gameState.getBoard().getField(startfieldnr,8));
        		startfieldnr++;
        	}
        }else{
        	while(Shapestack.empty() == false){
        		startmove.layStoneOntoField(Shapestack.pop(), gameState.getBoard().getField(startfieldnr,8));
        		startfieldnr++;
        	}        	
        }        
        
        
        
        //Gegneranalyse
      

        	//Ende Gegneranalyse

        
        Map<Stone, Field> mapping = startmove.getStoneToFieldMapping();
        
        for (Stone stone : mapping.keySet()) {
        	System.out.println(stone.getColor().name() + "-" + stone.getShape().name() + " -> " + 
        			mapping.get(stone).getPosX() + " " + mapping.get(stone).getPosY());
        }
        System.out.println("-------");
        
        startmove.addHint("Cunning gambit");
        
        if (startmove.getStoneToFieldMapping().size() >= 2) {
        	sendAction(startmove);
        	return true;
        } else {
        	return false;
        }
    }
    
   
    /**
     * Versucht einen Zug zu finden, indem f��r alle Steine auf der Hand alle
     * freien Felder des Spielbrettes durchgegangen werden und dann versucht
     * wird, ob das legen des Steines auf das freie Feld m��glich ist.
     * 
     * @return <code>true</code>: Wir m��ssen den ersten Zug machen <br>
     *         <code>false</code>: Wir m��ssen nicht den ersten Zug machen
     */
    private boolean tryToDoValidLayMove() {	
    	LayMove bestLayMove = null;
    	Integer bestScore = -1;
    	List<Field> fields = gameState.getBoard().getFields();
    	
    	for (int i = 0; true; i++) {
    		LayMove layMove = new LayMove();
        	List<Stone> ourStones = new ArrayList<Stone>(gameState.getCurrentPlayer().getStones());
        	
        	Collections.shuffle(ourStones);
        	Collections.shuffle(fields);
        	
        	while (ourStones.size() > 0) {
        		boolean moveFound = false;
        		
        		//System.out.println("Trying to find moves...");
        		
                for (Stone stone : ourStones) {
                    for (Field field : fields) {
                        if (field.isFree()) {
                            layMove.layStoneOntoField(stone, field);
                            
                            if (GameUtil.checkIfLayMoveIsValid(layMove,
                            		gameState.getBoard())) {
                            	//System.out.println("Laying stone " + stone.getColor().name() + " " + stone.getShape().name() +
                            	//		" on field " + field.getPosX() + ":" + field.getPosY());
                            	ourStones.remove(stone);
                            	
                            	moveFound = true;
                            	break;
                            } else {
                            	layMove.getStoneToFieldMapping().remove(stone);
                            }
                        }
                    }
                    
                    if (moveFound) break;
                }
                
                //System.out.printf("%d stones left", ourStones.size());
                
                if (!moveFound) break;
        	}
        	
        	
        	if (layMove.getStoneToFieldMapping().size() > 0) {
        		int score = PointsCalculator.getPointsForLayMove(layMove.getStoneToFieldMapping(), gameState.getBoard());
        		
        		if (score > bestScore) {
        			System.out.printf("#%d: New best move for %d points\n", i, score);
        			bestLayMove = layMove;
        			bestScore = score;
        		}
        	} else {
        		break;
        	}
        	
        	long delta_t = System.currentTimeMillis() - this.moveRequestedAt;
        	if (delta_t > 1600) {
        		
        		System.out.printf("Stopping after %d moves and %dms\n", i, delta_t);
        		
        		if (bestLayMove != null) {
        			//bestLayMove.addHint("Time", Long.toString(delta_t));
        			//bestLayMove.addHint("Moves tested", Integer.toString(i));
        			bestLayMove.addHint("Time: " + Long.toString(delta_t) + "ms; Moves tested: " + Integer.toString(i));		
        		}
        		
        		break;
        	}
    	}
        
    	if (bestScore >= 0) {
            sendAction(bestLayMove);
            System.out.printf("Sent move after %dms\n", System.currentTimeMillis() - this.moveRequestedAt);
            return true;
    	} else {
    		return false;
    	}
    }

    /**
     * Wechselt die ��bergebenen Steine aus und beendet damit die Runde.
     */
    private void exchangeStones(List<Stone> stones) {
        ExchangeMove exchangeMove = new ExchangeMove(stones);
        exchangeMove.addHint("Sending Exchange Move");
        sendAction(exchangeMove);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onUpdate(Player player, Player otherPlayer) {
        currentPlayer = player;

        System.out.println("*** Spielerwechsel: " + player.getPlayerColor());

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onUpdate(GameState gameState) {
        this.gameState = gameState;
        currentPlayer = gameState.getCurrentPlayer();
        otherPlayer = gameState.getOtherPlayer();

        System.out.print("*** Das Spiel geht voran: Zug = "
                + gameState.getTurn());
        System.out.println(", Spieler = " + currentPlayer.getPlayerColor());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void sendAction(Move move) {
        client.sendMove(move);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void gameEnded(GameResult data, PlayerColor color,
            String errorMessage) {

        System.out.println("*** Das Spiel ist beendet");
    }
}
